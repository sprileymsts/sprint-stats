# TODO:
# Update percentages with the REAL #.  Keep the timelines as they are.


import argparse, base64, json, os, sys, time, html, urllib.parse, urllib.request
from datetime import datetime, timedelta

# HELPER FUNCTIONS:
# Given a single date range (startDate -> endDate), break it up into multiple
# date ranges (if necessary) so that weekends are not included.
# Fridays are cut off at 11:59pm. Mondays start at 6:00am
def breakupWeekends(listOfRanges, summary, startDate, endDate, status):
    originalStartDate = datetime.strptime(startDate, '%Y-%m-%dT%H:%M:%S.%f%z')
    originalEndDate = datetime.strptime(endDate, '%Y-%m-%dT%H:%M:%S.%f%z')
    loopDate = datetime.strptime(startDate, '%Y-%m-%dT%H:%M:%S.%f%z')
    inWeekend = False
    startedBreak = False

    the_day_after_originalEndDate = (originalEndDate + timedelta(days=1)).day

    while (loopDate.day != the_day_after_originalEndDate):
        if (loopDate.weekday() > 4):
            if not inWeekend:
                inWeekend = True
                startedBreak = True
                tempDate = loopDate
                tempDate += timedelta(days=-1)
                tempDate = tempDate.replace(hour=23, minute=59, second=59)
                listOfRanges.append(summary + "#|#" + str(originalStartDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + str(tempDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + status)
        else:
            inWeekend = False
            if startedBreak:
                startedBreak = False
                tempDate = loopDate
                tempDate = tempDate.replace(hour=6, minute=0, second=0)
                originalStartDate = tempDate
        loopDate += timedelta(days=1)

    listOfRanges.append(summary + "#|#" + str(originalStartDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + str(originalEndDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + status)
    
    return str(originalEndDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z'))

# Used to implement the progress bar
def updt(total, progress):
    barLength, status = 50, ""
    progress = float(progress) / float(total)
    if progress >= 1.:
        progress, status = 1, "\r\n"
    block = int(round(barLength * progress))
    text = "\rProcessing stories... [{}] {:.0f}% {}".format(
        "#" * block + "-" * (barLength - block), round(progress * 100, 0),
        status)
    sys.stdout.write(text)
    sys.stdout.flush()



# MAIN PROGRAM:

# Define and parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("TeamName", help="Name of the team in JIRA in double quotes")
parser.add_argument("-noweekends", help="Don't show weekends", action="store_true")
args = parser.parse_args()

timespanEndDate = datetime.today().strftime('%Y-%m-%d')
timespanStartDate = (datetime.strptime(timespanEndDate, '%Y-%m-%d') - timedelta(days=21)).strftime('%Y-%m-%d')

SPRINT_START = (datetime.strptime(timespanEndDate, '%Y-%m-%d') - timedelta(days=21)).strftime('%Y-%m-%d') + "T08:00:00.000-0600"
if args.noweekends:
    NO_WEEKENDS = True
else:
    NO_WEEKENDS = False

# Get JIRA API credentials from environment variable
username = os.environ['JIRA_EMAIL']
password = os.environ['JIRA_KEY']


download_url = "https://msts-eng.atlassian.net/rest/api/latest/search?startAt=0&maxResults=200&jql=project+IN(" + urllib.parse.quote(args.TeamName) + ")+and+((status+changed+during+(startofDay(-14d)%2C+now())+FROM+(%22PO+Review%22)+TO+(%22Accepted%22))+or+(status+changed+during+(startofDay(-14d)%2C+now())+FROM+(%22In+Progress%22)+TO+(%22Resolved%22))+or+(status+in+(%22In+Progress%22%2C+%22In+Development%22%2C+%22In+Test%22%2C+%22In+Review%22%2C+%22PO+Review%22)))+ORDER+BY+updatedDate+ASC"
print(download_url)

issue_root_url = "https://msts-eng.atlassian.net/rest/api/latest/issue/"
issue_params = "?expand=changelog"

# Get list of stories for the given "Sprint Name"
req = urllib.request.Request(download_url)
credentials = ('%s:%s' % (username, password))
encoded_credentials = base64.b64encode(credentials.encode('ascii'))
req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))

data = []
blockStart = {}
with urllib.request.urlopen(req) as f:
    jsondata = json.loads(f.read().decode('utf-8'))
    for item in jsondata["issues"]:
        data.append(item)

if (jsondata["total"] > 100):
    download_url = download_url.replace("startAt=0", "startAt=100")
    req = urllib.request.Request(download_url)
    req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))
    with urllib.request.urlopen(req) as f:
        jsondata = json.loads(f.read().decode('utf-8'))
        for item in jsondata["issues"]:
            data.append(item)
if (jsondata["total"] > 200):
    download_url = download_url.replace("startAt=100", "startAt=200")
    req = urllib.request.Request(download_url)
    req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))
    with urllib.request.urlopen(req) as f:
        jsondata = json.loads(f.read().decode('utf-8'))
        for item in jsondata["issues"]:
            data.append(item)

# Set up values needed for Javascript + HTML
PROJECT_NAME = data[0]["fields"]["project"]["name"]
final_text = ""
final_text_incident = ""
final_text_dates = ""
output_text = ""
incidentlist = []

# Check to make sure multiple teams don't have same sprint name
names_set = set()
for names_index in range(len(data)):
    names_set.add(data[names_index]["fields"]["project"]["name"])

# Set up progress bar
runs = len(data)
run_num = 0
print("Found " + str(runs) + " items to process in for team(s): " + str(names_set))

# For each story in the given sprint...
for key in data:
    
    # Increment progress bar
    updt(runs, run_num + 1)
    run_num += 1

    # Ignore any sub-tasks
    if str(key["fields"].get("issuetype").get("name")) == "Sub-task":
        continue
    
    # Set up story/row title
    currentIssue = key["key"]
    currentPoints = str(key["fields"].get("customfield_10043"))
    if currentPoints == "None":
        currentPoints = "0"
    else:
        currentPoints = str(int(float(currentPoints)))
    
    # Handle the incidents
    if currentPoints == "0":
        incidentlist.append(html.escape(key["fields"]["updated"] + " " + key["fields"]["summary"].replace("'", "")) + " [<a href='https://msts-eng.atlassian.net/browse/" + key["key"] + "'>Link</a>]")
        continue

    currentSummary = "(" + currentPoints.zfill(2) + ") " + key["fields"]["summary"].replace("'", "")

    # Get story details for a given story id
    req = urllib.request.Request(issue_root_url + currentIssue + issue_params)
    credentials = ('%s:%s' % (username, password))
    encoded_credentials = base64.b64encode(credentials.encode('ascii'))
    req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))
    with urllib.request.urlopen(req) as f:
        issue_data = json.loads(f.read().decode('utf-8'))
    
    
    # If story was created after sprint started, don't use sprint start date
    if issue_data["fields"]["created"] > SPRINT_START:
        previous_time = issue_data["fields"]["created"]
    else:
        # if issuekey["items"][index]["toString"] == "In Development":
        #     # Fix for calculating percentages when story was In Development more than 3 weeks ago
        #     final_text_dates += "start_dates[\"" + currentSummary + "\"] = " + "moment(\"" + issue_data["fields"]["created"] + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate();"
        previous_time = SPRINT_START

    # Reverse the history so we see status changes from oldest to newest
    issue_key = issue_data["key"]
    issue_data = issue_data["changelog"]["histories"]
    issue_data.reverse()

    # Set up list that will hold all status changes
    mylist = []
    first_status_change = True

    # Unipay
    if "BCA" in args.TeamName:
        READY_TO_WORK = "Ready to Work"
    elif "PI" in args.TeamName:
        READY_TO_WORK = "Ready to Work"
    else:
        # Customer Portal
        READY_TO_WORK = "Ready"

    # For each entry in the history...
    for issuekey in issue_data:

        # If we have a status change...
        for index in range(len(issuekey["items"])):
            
            # Keep track of Blocked label
            if issuekey["items"][index]["field"] == "labels":
                if "blocked" in issuekey["items"][index]["toString"].lower():
                    blockStart[issue_key] = issuekey["created"]
                if "blocked" in issuekey["items"][index]["fromString"].lower():
                    if "blocked" not in issuekey["items"][index]["toString"].lower() == "":
                        if issue_key in blockStart:
                            mylist.append(currentSummary + "#|#" + str(blockStart[issue_key]) + "#|#" + str(issuekey["created"]) + "#|#Blocked")

            if issuekey["items"][index]["field"] == "status":

                # If this is the first status change, add the To Do status
                if (first_status_change):
                    if issuekey["created"] < SPRINT_START:
                        if issuekey["items"][index]["toString"] == "In Development":
                            # Fix for calculating percentages when story was In Development more than 3 weeks ago
                            final_text_dates += "start_dates[\"" + currentSummary + "\"] = " + "moment(\"" + issuekey["created"] + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate();"
                        issuekey["created"] = SPRINT_START
                        
                    
                    if (NO_WEEKENDS):
                        last_start_time = breakupWeekends(mylist, currentSummary, str(previous_time), str(issuekey["created"]), READY_TO_WORK)
                    else:
                        mylist.append(currentSummary + "#|#" + str(previous_time) + "#|#" + str(issuekey["created"]) + "#|#" + READY_TO_WORK)
                        last_start_time = str(issuekey["created"])
                    
                    first_status_change = False
                    last_status = issuekey["items"][index]["toString"]

                    # Generate Done status
                    if issuekey["items"][index]["toString"] == "Done":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Done")

                    # Generate Closed status
                    if issuekey["items"][index]["toString"] == "Closed":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Closed")    

                    # Generate Resolved status
                    if issuekey["items"][index]["toString"] == "Resolved":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Resolved") 

                    # Generate Accepted status
                    if issuekey["items"][index]["toString"] == "Accepted":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Accepted") 

                # Else it's not the first status change  
                else:
                    if last_start_time < SPRINT_START:
                        last_start_time = SPRINT_START
                    if issuekey["created"] < SPRINT_START:
                        if issuekey["items"][index]["toString"] == "In Development":
                            # Fix for calculating percentages when story was In Development more than 3 weeks ago
                            final_text_dates += "start_dates[\"" + currentSummary + "\"] = " + "moment(\"" + issuekey["created"] + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate();"
                        issuekey["created"] = SPRINT_START
                    
                    if (NO_WEEKENDS):
                        last_start_time = breakupWeekends(mylist, currentSummary, str(last_start_time), str(issuekey["created"]), last_status)
                    else:
                        mylist.append(currentSummary + "#|#" + str(last_start_time) + "#|#" + str(issuekey["created"]) + "#|#" + last_status)
                        last_start_time = str(issuekey["created"])
                    
                    previous_time = issuekey["created"]
                    last_status = issuekey["items"][index]["toString"]
                    
                    # Generate Done status
                    if issuekey["items"][index]["toString"] == "Done":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Done")

                    # Generate Closed status
                    if issuekey["items"][index]["toString"] == "Closed":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Closed")

                    # Generate Resolved status
                    if issuekey["items"][index]["toString"] == "Resolved":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Resolved")

                    # Generate Accepted status
                    if issuekey["items"][index]["toString"] == "Accepted":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Accepted")

    # Fix to show stories that don't move past In Development
    if last_status == "In Development":
        if (NO_WEEKENDS):
            breakupWeekends(mylist, currentSummary, str(previous_time), str(timespanEndDate + "T23:59:59.000-0500"), last_status)
        else:
            mylist.append(currentSummary + "#|#" + previous_time + "#|#" + timespanEndDate + "T23:59:59.000-0500" + "#|#" + last_status)
        # mylist.append(currentSummary + "#|#" + previous_time + "#|#" + args.KanbanEndDate + "T23:59:59.000-0500" + "#|#" + last_status)

    # Save the text that will go into Javascript file
    for x in range(len(mylist)): 
        output_issue, output_start, output_end, output_status = mylist[x].split("#|#")
        output_text = "["
        output_text += "'" + output_issue + "', "
        output_text += "'" + output_status + "', "
        output_text += "moment(\"" + output_start + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate(), "
        output_text += "moment(\"" + output_end + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate()"
        output_text += "]"
        output_text += ","
        final_text += output_text + '\n'

incidentlist.sort()
for x in range(len(incidentlist)): 
    final_text_incident += '<li>' + incidentlist[x][29:] + ' (0 pts)</li>'

# Generate the Javascript file
output_js_file = open("sprint_output.js","w")
output_js_file.write("var sprint_output = [" + final_text[:-1] + "];\n")
output_js_file.write("document.getElementById(\"sprint-title\").innerHTML = \"Kanban Report: Stories Accepted in the last 14 days\";\n")
output_js_file.write("document.getElementById(\"incidents\").innerHTML = \"" + final_text_incident[:-1] + "\";\n")
output_js_file.write("let start_dates = {};\n")
output_js_file.write(final_text_dates)
output_js_file.close()
